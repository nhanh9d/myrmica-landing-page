// var controller
// // detect if mobile browser. regex -> http://detectmobilebrowsers.com
// var isMobile = (function (a) {
//   return (
//     /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(
//       a
//     ) ||
//     /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
//       a.substr(0, 4)
//     )
//   )
// })(navigator.userAgent || navigator.vendor || window.opera)

// if (isMobile) {
//   var myScroll
//   $(document).ready(function () {
//     // wrap for iscroll
//     $('#content-wrapper')
//       .addClass('scrollContainer')
//       .wrapInner('<div class="scrollContent"></div>')

//     // add iScroll
//     myScroll = new IScroll('#content-wrapper', {
//       scrollX: false,
//       scrollY: true,
//       scrollbars: true,
//       useTransform: false,
//       useTransition: false,
//       probeType: 3,
//       click: true,
//     })

//     // update container on scroll
//     myScroll.on('scroll', function () {
//       controller.update()
//     })

//     // overwrite scroll position calculation to use child's offset instead of parents scrollTop();
//     controller.scrollPos(function () {
//       return -myScroll.y
//     })

//     // refresh height, so all is included.
//     setTimeout(function () {
//       myScroll.refresh()
//     }, 0)

//     // manual set hight (so height 100% is available within scroll container)
//     $(document).on('orientationchange', function () {
//       $('section')
//         .css('min-height', $(window).height())
//         .parent('.scrollmagic-pin-spacer')
//         .css('min-height', $(window).height())
//     })
//     $(document).trigger('orientationchange') // trigger to init
//   })
//   // init the controller
//   controller = new ScrollMagic.Controller({
//     container: '#content-wrapper',
//     globalSceneOptions: {
//       triggerHook: 'onLeave',
//     },
//   })
// } else {
//   // init the controller
//   controller = new ScrollMagic.Controller({
//     globalSceneOptions: {
//       triggerHook: 'onLeave',
//     },
//   })
// }

var app = {
  textByLang: {
    vi: {
      Web:
        "Tại Myrmica, chúng tôi tập trung vào các phương pháp hay nhất hiện tại của ngành công nghiệp web cho tất cả các bản dựng trang web của chúng tôi. Điều đó bao gồm thiết kế đáp ứng trên mọi dự án cũng như các biện pháp bảo mật cao trên mọi trang web. Tất cả các giải pháp của chúng tôi đều bao gồm chứng chỉ SSL để cho khách truy cập trang web của bạn thấy trang web của bạn được bảo mật và sự an toàn và bảo mật của họ là quan trọng đối với bạn.",
      AI:
        "Ứng dụng AI đang làm cho công nghệ trở nên thông minh hơn, thân thiện hơn và linh hoạt hơn. Mycmica tập trung vào các giải pháp Computer Vision, Speech, NLP .. tiên tiến nhất, với mục tiêu nâng cao và thay đổi trải nghiệm cuộc sống của chúng ta.",
    },
    en: {
      Web:
        "At Myrmica we focus on current web industry best practices for all of our website builds.  That includes responsive design on every project as well as high security measures on every site.  All of our solutions include SSL certificates which shows your website visitors your website is secure and that their safety and security are important to you.",
      AI: `AI application is making technology smarter, friendlier and more flexible. 
        Mycmica focuses on the most advanced Computer Vision, Speech, NLP .. solutions, 
        with the goal of enhancing and changing our life experiences.`,
    },
    jv: {
      AI:
        "Ứng dụng AI đang làm cho công nghệ trở nên thông minh hơn, thân thiện hơn và linh hoạt hơn. Mycmica tập trung vào các giải pháp Computer Vision, Speech, NLP .. tiên tiến nhất, với mục tiêu nâng cao và thay đổi trải nghiệm cuộc sống của chúng ta.",
    },
  },

  init: () => {
    app.initCarousel();
    app.setTextByLanguage(null, "vi");
    app.initScrollListener();
  },

  /**
   * Khởi tạo carousel
   */
  initCarousel: () => {
    $(".slick-carousel").slick({
      centerMode: true,
      centerPadding: "60px",
      slidesToShow: 3,
      infinite: true,
      arrows: false,
      autoplay: true,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            centerMode: true,
            centerPadding: "40px",
            slidesToShow: 3,
          },
        },
        {
          breakpoint: 480,
          settings: {
            centerMode: true,
            centerPadding: "40px",
            slidesToShow: 1,
          },
        },
      ],
    });
    $(".slick-carousel-vertical").slick({
      slidesToShow: 3,
      infinite: true,
      arrows: false,
      autoplay: true,
      vertical: true,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            vertical: false,
          },
        },
      ],
    });
  },

  initScrollListener: () => {
    $(document).on("wheel mousedown keydown", (e) => {
      if (
        e.type == "wheel" ||
        e.target.tagName == "HTML" ||
        (e.type == "keydown" &&
          ((e.ctrlKey && (e.keyCode == 36 || e.which == 35)) ||
            (!e.ctrlKey &&
              (e.keyCode == 33 ||
                e.which == 34 ||
                e.which == 38 || //mũi tên lên
                e.which == 37 || //mũi tên trái
                e.which == 39 || //mũi tên phải
                e.which == 40)))) //mũi tên xuống
      ) {
        let isScrollUp = false;
        //cuộn chuột
        if (e.type === "wheel") {
          isScrollUp = e.originalEvent.deltaY < 0;
        }
        if (e.type === "keydown") {
          //mũi tên xuống
          if ([39, 40].includes(e.which)) {
            isScrollUp = false;
          }
          //mũi tên lên
          else {
            isScrollUp = true;
          }
        }
        var currentSection = document.querySelector(".section.top");
        var currentSectionId = currentSection.id;
        var currentSectionPos = parseInt(currentSectionId.split("-")[1]);
        var nextSectionPos = isScrollUp
          ? currentSectionPos - 1 //scroll lên
          : currentSectionPos + 1; //scroll xuống
        app.changeSection(nextSectionPos);

        return false;
      }
    });
  },

  /**
   * event khi có nút bấm chuyển section được bấm
   * @param {Event} event event của nút bấm
   * @param {number} sectionNum section cần chuyển đến
   */
  toSection: (event, sectionNum) => {
    event.preventDefault();
    var activeLi = undefined;
    //Chỉnh lại active nav bar
    if ([1, 2, 3, 4, 6].includes(sectionNum)) {
      activeLi = document.querySelector(".nav-item.active");
      activeLi.classList.remove("active");
      activeLi = undefined;
    }
    if (sectionNum === 1) {
      //Về trang chủ
      activeLi = document.querySelector('.nav-item[data-nav-section="1"]');
    }
    if ([2, 3, 4].includes(sectionNum)) {
      //Các trang dịch vụ
      activeLi = document.querySelector('.nav-item[data-nav-section="234"]');
    }
    if (sectionNum === 6) {
      //Về trang chủ
      activeLi = document.querySelector('.nav-item[data-nav-section="6"]');
    }

    if (typeof activeLi != "undefined" && activeLi != null) {
      activeLi.classList.add("active");
    }

    app.changeSection(sectionNum);
  },

  /**
   * Chuyển section
   * @param {number} sectionNum
   */
  changeSection: (sectionNum) => {
    var totalSection = document.querySelectorAll("section.section");
    var currentSection = document.querySelector(".section.top");
    var nextSectionId = `section-${sectionNum}`;

    if (sectionNum > 0 && sectionNum <= totalSection.length) {
      currentSection.classList.remove("top");
      document.getElementById(nextSectionId).classList.add("top");
    }
  },

  /**
   * Đổi ngôn ngữ
   * @param {string} lang vi, en, jv
   */
  setTextByLanguage: (event, lang) => {
    //stop event
    if (event) {
      event.preventDefault();
    }

    var name = "Vi";
    if (lang === "en") {
      name = "En";
    }
    if (lang === "jv") {
      name = "Jp";
    }

    $(".dropdown-myrmica span").html(name);
    console.log(app.textByLang[lang]);
  },
};
app.init();
