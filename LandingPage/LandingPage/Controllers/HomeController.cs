﻿using LandingPage.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace LandingPage.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string SubmitContact()
        {
            var result = "success";
            try
            {
                var item = JsonConvert.SerializeObject(new
                {
                    fullname = Request.Form["fullname"][0],
                    phone = Request.Form["phone"][0],
                    email = Request.Form["email"][0],
                    content = Request.Form["content"][0],
                });

                if (!System.IO.Directory.Exists($"{AppContext.BaseDirectory}/Data"))
                {
                    System.IO.Directory.CreateDirectory($"{AppContext.BaseDirectory}/Data");
                }

                System.IO.File.AppendAllTextAsync($"{AppContext.BaseDirectory}/Data/contact.txt", $"{item}{Environment.NewLine}");

            }
            catch (Exception)
            {
                result = "fail";
            }
            return result;
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
