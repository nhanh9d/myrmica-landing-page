// var controller
// // detect if mobile browser. regex -> http://detectmobilebrowsers.com
// var isMobile = (function (a) {
//   return (
//     /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(
//       a
//     ) ||
//     /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
//       a.substr(0, 4)
//     )
//   )
// })(navigator.userAgent || navigator.vendor || window.opera)

// if (isMobile) {
//   var myScroll
//   $(document).ready(function () {
//     // wrap for iscroll
//     $('#content-wrapper')
//       .addClass('scrollContainer')
//       .wrapInner('<div class="scrollContent"></div>')

//     // add iScroll
//     myScroll = new IScroll('#content-wrapper', {
//       scrollX: false,
//       scrollY: true,
//       scrollbars: true,
//       useTransform: false,
//       useTransition: false,
//       probeType: 3,
//       click: true,
//     })

//     // update container on scroll
//     myScroll.on('scroll', function () {
//       controller.update()
//     })

//     // overwrite scroll position calculation to use child's offset instead of parents scrollTop();
//     controller.scrollPos(function () {
//       return -myScroll.y
//     })

//     // refresh height, so all is included.
//     setTimeout(function () {
//       myScroll.refresh()
//     }, 0)

//     // manual set hight (so height 100% is available within scroll container)
//     $(document).on('orientationchange', function () {
//       $('section')
//         .css('min-height', $(window).height())
//         .parent('.scrollmagic-pin-spacer')
//         .css('min-height', $(window).height())
//     })
//     $(document).trigger('orientationchange') // trigger to init
//   })
//   // init the controller
//   controller = new ScrollMagic.Controller({
//     container: '#content-wrapper',
//     globalSceneOptions: {
//       triggerHook: 'onLeave',
//     },
//   })
// } else {
//   // init the controller
//   controller = new ScrollMagic.Controller({
//     globalSceneOptions: {
//       triggerHook: 'onLeave',
//     },
//   })
// }

var app = {
    textByLang: {
        // ================================================================================
        vi: {

            // Navigation bar
            HomeLink: "Trang Chủ",
            ServiceLink: "Dịch Vụ",
            AboutUsLink: "Về Chúng Tôi",
            ContactLink: "Liên Hệ",

            // Home
            Section1LeadingText: "GIẢI PHÁP CÔNG NGHỆ",
            Section1FollowingText: "TOÀN DIỆN",
            Section1FirstMobileTitle: "PHÁT TRIỂN",
            Section1SecondMobileTitle: "ỨNG DỤNG DI ĐỘNG",
            Section1FirstWebTitle: "PHÁT TRIỂN",
            Section1SecondWebTitle: "WEBSITE",
            Section1FirstAiTitle: "GIẢI PHÁP",
            Section1SecondAiTitle: "TRÍ TUỆ NHÂN TẠO",

            Section1IotTitle: "GIẢI PHÁP IoT",
            Section1FirstDataTitle: "PHÂN TÍCH",
            Section1SecondDataTitle: "DỮ LIỆU",

            Section1Scroll: "TRƯỢT",

            // Mobile
            Section2LeadingText: "Phát triển",
            Section2FollowingText: "Ứng dụng di động",
            Section2Description: "Đội ngũ của chúng tôi nắm bắt được nhiều công nghệ và có kinh nghiệm phát triển trên cả hai nền tảng IOS và Android. Myrmica luôn cố gắng hiểu rõ hơn về khách hàng cũng như nâng cao thương hiệu của bạn. Chúng tôi tin rằng ứng dụng di động cùng với một trải nghiệm khách hàng tốt có thể mang lại giá trị đích thực cho doanh nghiệp của bạn.",
            Section2MoreInfo: "Tìm hiểu thêm",
            Section2Scroll: "TRƯỢT",

            // Website
            Section3LeadingText: "Xây dựng website",
            // Section3FollowingText:
            //     "Website",
            Section3Description: "Tại Myrmica, chúng tôi tập trung vào các phương pháp hay nhất hiện tại của ngành công nghiệp website cho tất cả các bản dựng trang website của chúng tôi. Điều đó bao gồm thiết kế đáp ứng trên mọi dự án cũng như các biện pháp bảo mật cao trên mọi trang website. Tất cả các giải pháp của chúng tôi đều bao gồm chứng chỉ SSL để cho khách truy cập trang website của bạn thấy trang website của bạn được bảo mật và sự an toàn và bảo mật của họ là quan trọng đối với bạn.",
            Section3MoreInfo: "Tìm hiểu thêm",
            Section3Scroll: "TRƯỢT",

            // AI
            Section4LeadingText: "Giải pháp",
            Section4FollowingText: "Trí tuệ nhân tạo",
            Section4Description: "Ứng dụng AI đang làm cho công nghệ trở nên thông minh hơn, thân thiện hơn và linh hoạt hơn. Mycmica tập trung vào các giải pháp Computer Vision, Speech Recognition, NLP tiên tiến nhất, với mục tiêu nâng cao và thay đổi trải nghiệm cuộc sống của chúng ta.",
            Section4MoreInfo: "Tìm hiểu thêm",
            Section4Scroll: "TRƯỢT",

            // IoT
            Section7LeadingText:
                "Giải pháp IoT",
            // Section3FollowingText:
            //     "Website",
            Section7Description:
                "IoT được biết đến như là một công nghệ mới nhất hiện nay. Mọi thứ xung quanh chúng ta sẽ được kết nối thông qua Internet. Với những công nghệ IoT tiên tiến nhất, Myrmica tin rằng chúng tôi có thể thay đổi thế giới",
            Section7MoreInfo:
                "Tìm hiểu thêm",
            Section7Scroll:
                "TRƯỢT",

            // contact
            Section5LeadingText: "LIÊN HỆ VỚI CHÚNG TÔI ĐỂ ĐƯỢC",
            Section5FollowingText: "TƯ VẤN MIỄN PHÍ",
            Section5SendInfo: "GỬI THÔNG TIN",
            Section5Scroll: "TRƯỢT",
            Section5FormFullName: "Họ và tên",
            Section5FormPhonenumber: "Số điện thoại",
            Section5FormEmail: "Email",
            Section5FormRequest: "Yêu cầu của bạn với đội ngũ Myrmica",
            Section5LeadingText: "LIÊN HỆ VỚI CHÚNG TÔI ĐỂ ĐƯỢC",
            Section5FollowingText: "TƯ VẤN MIỄN PHÍ",
            Section5SendInfo: "GỬI THÔNG TIN",
            Section5Scroll: "TRƯỢT",

            // about us
            Section6LeadingText: "Đội ngũ <span>Myrmica</span>",
            Section6FollowingText: "Đây là 1 đội ngũ gồm các bạn trẻ nhiệt huyết và đam mê với công nghệ, mong muốn có thể góp phần sức lực của mình vào công cuộc đổi mới của cách mạng công nghiệp 4.0.",
            Section6Scroll: "Trượt",

            // about us footer
            Section6FooterCompanyName: "CÔNG TY CỔ PHẦN CÔNG NGHỆ MYRMICA",
            Section6FooterCompanyAddress: "<strong>Địa chỉ</strong>:Số 7, Biệt thự 2, khu đô thị Văn Khê, P.La Khê, Q.Hà Đông. Tp Hà Nội",
            Section6FooterCompanyLicense: "<strong>Giấy phép kinh doanh</strong>: 0109521755",
            Section6FooterCompanyService: "DỊCH VỤ",
            Section6FooterCompanyMobileService: "Phát triển ứng dụng di động",
            Section6FooterCompanyWebService: "Xây dựng website",
            Section6FooterCompanyAiService: "Ứng dụng trí tuệ nhân tạo",
            Section6FooterAboutUs: "VỀ CHÚNG TÔI",
            Section6FooterAboutUsLink: "Về Myrmica",
            Section6FooterMoreInfo: "KẾT NỐI VỚI MYRMICA",

        },

        // ================================================================================
        en: {

            // Navigation bar
            HomeLink: "Home",
            ServiceLink: "Service",
            AboutUsLink: "About us",
            ContactLink: "Contact",

            // Home
            Section1LeadingText: "ALL-ROUND",
            Section1FollowingText: "TECHNICAL SOLUTION",
            Section1FirstMobileTitle: "MOBILE APP",
            Section1SecondMobileTitle: "DEVELOPMENT",
            Section1FirstWebTitle: "WEBSITE",
            Section1SecondWebTitle: "DEVELOPMENT",
            Section1FirstAiTitle: "ARTIFICIAL INTELLIGENCE",
            Section1SecondAiTitle: "SOLUTION",

            Section1IotTitle: "IoT SOLUTION",
            Section1FirstDataTitle: "DATA",
            Section1SecondDataTitle: "ANALYTICS",

            Section1Scroll: "Scroll",

            // Mobile
            Section2LeadingText: "Mobile app",
            Section2FollowingText: "Development",
            Section2Description: "Our development team holds various technologies and development experience on both platforms IOS and Android. We thrive to understand your customer better and improve your brand perception. Mobile App and a great User experience can bring true value to your business",
            Section2MoreInfo: "More Information",
            Section2Scroll: "Scroll",

            // Website
            Section3LeadingText: "Website Development",
            // Section3FollowingText:
            //     "Development",
            Section3Description: "At Myrmica we focus on current website industry best practices for all of our website builds.  That includes responsive design on every project as well as high security measures on every site.  All of our solutions include SSL certificates which shows your website visitors your website is secure and that their safety and security are important to you.",
            Section3MoreInfo: "More Information",
            Section3Scroll: "Scroll",

            // AI
            Section4LeadingText: "Artificial Intelligence",
            Section4FollowingText: "Solutions",
            Section4Description: "AI application is making technology smarter, friendlier and more flexible. Mycmica focuses on the most advanced Computer Vision, Speech Recognition, NLP solutions, with the goal of enhancing and changing our life experiences.",
            Section4MoreInfo: "More Information",
            Section4Scroll: "Scroll",

            // IoT
            Section7LeadingText:
                "IoT Solutions",
            // Section3FollowingText:
            //     "Development",
            Section7Description:
                "IoT is said to be the latest technology. Everything around us is connected via the Internet. How convenient will our world be? Myrmica’s cutting edge IoT technology outsourcing will change the world.",
            Section7MoreInfo:
                "More Information",
            Section7Scroll:
                "Scroll",

            // contact
            Section5LeadingText: "Contact with us to get",
            Section5FollowingText: "free consulation",
            Section5SendInfo: "Submit",
            Section5Scroll: "Scroll",
            Section5FormFullName: "Full Name",
            Section5FormPhonenumber: "Phone Number",
            Section5FormEmail: "Email",
            Section5FormRequest: "Your Request to Myrmica team",

            // about us
            Section6LeadingText: "<span>Myrmica</span> Team",
            Section6FollowingText: "Myrmica is a tech team consisting of enthusiastic young programmers. We hope to contribute our skills and knowledge to the development of the Fourth Industrial Convolution.",
            Section6Scroll: "Scroll",

            // about us footer
            Section6FooterCompanyName: "MYRMICA TECHNOLOGY JOINT STOCK COMPANY",
            Section6FooterCompanyAddress: "<strong>Address</strong>: No. 7, Villa 2, Van Khe urban area, La Khe ward, Ha Dong district. Hanoi city",
            Section6FooterCompanyLicense: "<strong>Business license</strong>: 0109521755",
            Section6FooterCompanyService: "SERVICES",
            Section6FooterCompanyMobileService: "Mobile Application Development",
            Section6FooterCompanyWebService: "Website Development",
            Section6FooterCompanyAiService: "Artificial Intelligent Solution",
            Section6FooterAboutUs: "About us",
            Section6FooterAboutUsLink: "About Myrmica",
            Section6FooterMoreInfo: "CONNECT WITH MYRMICA",

        },

        // ================================================================================
        jv: {

            // Navigation bar
            HomeLink: "ホーム",
            ServiceLink: "サービス",
            AboutUsLink: "企業情報",
            ContactLink: "お問い合わせ",

            // Home
            Section1LeadingText: "トータル",
            Section1FollowingText: "技術ソリューション",
            Section1FirstMobileTitle: "モバイルアプリ",
            Section1SecondMobileTitle: "開発",
            Section1WebTitle: "ウェブ開発",
            Section1FirstWebTitle: "ウェブサイト",
            Section1SecondWebTitle: "開発",
            Section1FirstAiTitle: "人工知能",
            Section1SecondAiTitle: "ソリューション",

            Section1IotTitle: "IoT ソリューション",
            Section1FirstDataTitle: "デ-タ",
            Section1SecondDataTitle: "分析",

            Section1Scroll: "スクロール",

            // Mobile
            Section2LeadingText: "モバイルアプリ",
            Section2FollowingText: "開発",
            Section2Description: "弊社の開発チームはIOSとANDROIDのプラットフォーム開発の経験があります。貴社の顧客をより深く理解し、同時に貴社のブランド認知を向上することが弊社の目標です。優れたモバイルアプリとカスタマーエクスペリエンスによって貴社のビジネスに更なる価値を与えます。",
            Section2MoreInfo: "さらに詳しく",
            Section2Scroll: "スクロール",

            // Website
            Section3LeadingText: "ウェブサイト開発",
            Section3Description: "弊社はすべてのウェブサイト開発に関する現在の業界のベストプラクティスに焦点を当てています。これには、すべてのプロジェクトでのレスポンシブデザインと、すべてのサイトでの高度なセキュリティ対策が含まれます。弊社のすべてのソリューションには、ウェブサイトの訪問者にウェブサイトが安全であり、その安全性とセキュリティが重要であることを示すSSL証明書が含まれています。",
            Section3MoreInfo: "さらに詳しく",
            Section3Scroll: "スクロール",

            // AI弊社
            Section4LeadingText: "人工知能",
            Section4FollowingText: "ソリューション",
            Section4Description: "人工知能アプリケーションは、テクノロジーをよりスマートに、より親しみやすく、より柔軟にします。 弊社は、私たちの生活体験を向上させ、変化させることを目的として、最先端のコンピュータービジョン、自然言語処理ソリューションに焦点を当てています。",
            Section4MoreInfo: "さらに詳しく",
            Section4Scroll: "スクロール",

            // IoT
            Section7LeadingText:
                "IoT ソリューション",
            // Section3FollowingText:
            //     "Development",
            Section7Description:
                "最新のテクノロジーと言われています。私たちを取り囲む全てはインターネットによって繋がっています。私たちの世界はなんて便利なんでしょうか。弊社は、最先端のIoTテクノロジーアウトソーシングは世界を変えていくでしょう。",
            Section7MoreInfo:
                "さらに詳しく",
            Section7Scroll:
                "スクロール",

            // contact
            Section5LeadingText: "ご質問などございましたら",
            Section5FollowingText: "お気軽にお問い合わせください",
            Section5SendInfo: "送信",
            Section5Scroll: "スクロール",
            Section5FormFullName: "名前",
            Section5FormPhonenumber: "電話番号",
            Section5FormEmail: "メール",
            Section5FormRequest: "Myrmicaチームにリクエスト",

            // about us
            Section6LeadingText: "<span>Myrmica</span>チーム",
            Section6FollowingText: "Myrmicaは、熱心な若いプログラマーで構成される技術チームです。私たちは、私たちのスキルと知識を第4次産業コンボリューションの発展に貢献したいと考えています。",
            Section6Scroll: "スクロール",

            // about us footer
            Section6FooterCompanyName: "MYRMICA 株式会社",
            Section6FooterCompanyAddress: "<strong>住所</strong>: ハノイ市, ハドン区, ヴァンケ市街地2, 7号",
            Section6FooterCompanyLicense: "<strong>営業許可</strong>: 0109521755",
            Section6FooterCompanyService: "サービス",
            Section6FooterCompanyMobileService: "モバイルアプリケーション開発",
            Section6FooterCompanyWebService: "ウェブサイト開発",
            Section6FooterCompanyAiService: "人工知能アプリケーション",
            Section6FooterAboutUs: "詳しく",
            Section6FooterAboutUsLink: "Myrmicaについて",
            Section6FooterMoreInfo: "MYRMICAとつながる",

        },
    },

    isScrolling: false,
    sectionPie: undefined,
    allowToScroll: true,
    touchPointY: undefined,

    /**
     * Kiểm tra device có phải là mobile
     */
    isMobile: (function(a) {
        return (
            /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(
                a
            ) ||
            /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
                a.substr(0, 4)
            )
        )
    })(navigator.userAgent || navigator.vendor || window.opera),

    /**
     * config cho section navigator mỏ mobile
     */
    sectionPieConfig: {
        type: 'pie',
        data: {
            datasets: [{
                data: [
                    120,
                    120,
                    120
                ],
                backgroundColor: [
                    '#8e8e8e',
                    '#8e8e8e',
                    '#8e8e8e'
                ],
                label: 'Dataset 1',
                hoverBackgroundColor: '#ef2821'
            }],
            labels: [
                'Mobile',
                'Website',
                'AI'
            ]
        },
        options: {
            responsive: true,
            legend: {
                display: false
            },
            tooltips: {
                enabled: false
            },
            onClick: (e, pies) => {
                app.changeSection(pies[0]._index + 2);
            }
        }
    },

    init: () => {
        app.initCarousel();
        app.setTextByLanguage(null, "en");
        app.initScrollListener();
        app.initChartSection();


        //Đặt lại height của mobile
        if (app.isMobile) {
            const rootElement = document.querySelectorAll(".section")
            const viewPortH = rootElement[0].getBoundingClientRect().height;
            const windowH = window.innerHeight;
            const browserUiBarsH = viewPortH - windowH;
            for (var i = 0; i < rootElement.length; i++) {
                rootElement[i].style.height = `calc(100vh - ${browserUiBarsH}px)`;
            }
        }

        //Bỏ overlay
        $(document).ready(() => {
            setTimeout(() => {
                $('#overlay').fadeOut();
            }, 2000);
        });
    },

    initChartSection: () => {
        var ctx = document.getElementById('chart-area').getContext('2d');
        app.sectionPie = new Chart(ctx, app.sectionPieConfig);

        ////setImage
        //var cx = app.sectionPie.width / 2;
        //var cy = app.sectionPie.height / 2;
        //ctx.translate(cx, cy);

        //var startAngle = 0;
        //var endAngle = 120;
        //var midAngle = startAngle + (endAngle - startAngle) / 2;

        //// rotate by the midAngle
        //ctx.rotate(midAngle);

        //// given the donut radius (innerRadius) and donut radius (radius)
        ////var midWedgeRadius = app.sectionPie.innerRadius + (app.sectionPie.radius - app.sectionPie.innerRadius) / 2;
        ////context.translate(midWedgeRadius, 0);

        //var theImage = new Image(40, 40);
        //theImage.src = '../images/mobile-icon-small.png';
        //theImage.onload = e => {
        //    // given the image width & height
        //    ctx.drawImage(theImage, -theImage.width / 2, -theImage.height / 2);

        //    // undo translate & rotate
        //    ctx.setTransform(1, 0, 0, 1, 0, 0);
        //};
    },

    /**
     * Khởi tạo carousel
     */
    initCarousel: () => {
        $(".slick-carousel").slick({
            centerMode: true,
            centerPadding: "60px",
            slidesToShow: 3,
            infinite: true,
            arrows: true,
            // autoplay: true,
            nextArrow: '<button class="slick-carousel-button slick-carousel__next-button"> &rarr; </button>',
            prevArrow: '<button class="slick-carousel-button slick-carousel__previous-button"> &larr; </button>',
            responsive: [{
                    breakpoint: 768,
                    settings: {
                        centerMode: true,
                        centerPadding: "40px",
                        slidesToShow: 3,
                        arrows: false
                    },
                },
                {
                    breakpoint: 480,
                    settings: {
                        centerMode: true,
                        centerPadding: "0px",
                        slidesToShow: 3,
                        arrows: false
                    },
                },
            ],
        });
        $(".slick-carousel-vertical").slick({
            slidesToShow: 3,
            infinite: true,
            arrows: true,
            autoplay: false,
            // autoplay: true,
            vertical: true,
            touchMove: true,
            nextArrow: '<button class="slick-carousel-button slick-carousel-vertical__next-button"> &uarr; </button>',
            prevArrow: '<button class="slick-carousel-button slick-carousel-vertical__previous-button"> &darr; </button>',
            responsive: [{
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3,
                        arrows: false
                    },
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 3,
                        centerPadding: "100px",
                        vertical: false,
                        arrows: false
                    },
                },
            ],
        });
    },

    /**
     * tương tác với scroll
     */
    initScrollListener: () => {
        $(document).on("wheel keydown scroll", (e) => {
            // $(document).on("wheel keydown scroll touchmove", (e) => {
            console.log(e)
            if (
                e.type == "wheel" ||
                // e.type == "touchmove" ||
                e.target.tagName == "HTML" ||
                (e.type == "keydown" &&
                    ((e.ctrlKey && (e.keyCode == 36 || e.which == 35)) ||
                        (!e.ctrlKey &&
                            (e.keyCode == 33 ||
                                e.which == 34 ||
                                e.which == 38 || //mũi tên lên
                                e.which == 37 || //mũi tên trái
                                e.which == 39 || //mũi tên phải
                                e.which == 40)))) //mũi tên xuống
            ) {
                if (app.allowToScroll) {
                    if (e.type === "wheel" || e.type === "keydown") {
                        //Ngăn cản việc bị scroll nhanh quá
                        app.allowToScroll = false;
                    }

                    //Gán biến kiểm tra xem scroll lên hay xuống
                    let isScrollUp = false;
                    //cuộn chuột
                    if (e.type === "wheel") {
                        //Sau 0.5s thì lại cho scroll
                        setTimeout(() => {
                            app.allowToScroll = true;
                        }, 500);
                        isScrollUp = e.originalEvent.deltaY < 0;
                    }
                    if (e.type === "keydown") {
                        //mũi tên xuống
                        if ([39, 40].includes(e.which)) {
                            isScrollUp = false;
                        }
                        //mũi tên lên
                        else {
                            isScrollUp = true;
                        }
                    }

                    // if (e.type === "touchmove") {
                    //     if (app.touchPointY !== undefined) {
                    //         app.allowToScroll = false
                    //         isScrollUp = e.touches[0].clientY > app.touchPointY
                    //         console.log(e.touches[0].clientY)
                    //     } else {
                    //         app.touchPointY = e.touches[0].clientY
                    //         console.log(`assign touch point ${app.touchPointY}`)
                    //         return false
                    //     }
                    // }
                    var currentSection = document.querySelector(".section.top");
                    var currentSectionId = currentSection.id;
                    var currentSectionPos = parseInt(currentSectionId.split("-")[1]);
                    // var nextSectionPos = isScrollUp
                    //     ? currentSectionPos - 1 //scroll lên
                    //     : currentSectionPos + 1; //scroll xuống

                    var nextSectionPos;
                    if (isScrollUp) {
                        if (currentSectionPos == 7) {
                            nextSectionPos = 4; // from Iot -> Ai
                        } else if (currentSectionPos == 5) {
                            nextSectionPos = 8; // from Contact to DA
                        } else {
                            nextSectionPos = currentSectionPos - 1;
                        }
                    } else {
                        if (currentSectionPos == 4) {
                            nextSectionPos = 7; // from Ai to IoT 
                        } else if (currentSectionPos == 8) {
                            nextSectionPos = 5; // from DA to Contact
                        } else if (currentSectionPos == 6) {
                            nextSectionPos = 6; // Not change
                        } else {
                            nextSectionPos = currentSectionPos + 1;
                        }
                    }

                    //Đổi vị trí slide của về chúng tôi và liên hệ
                    //Cho slide liên hệ xuống dưới cùng
                    //if (nextSectionPos === 4 && isScrollUp) {
                    //    nextSectionPos = 6
                    //}
                    //if (nextSectionPos === 5 && !isScrollUp) {
                    //    nextSectionPos = isScrollUp ? 4 : 6
                    //}
                    //if (nextSectionPos === 7) {
                    //    nextSectionPos = 5
                    //}
                    app.changeSection(nextSectionPos);

                    return false;
                }
            }
            //đối với event scroll
            // if (e.type === 'touchmove') {

            //     if (!app.isScrolling) {
            //         window.requestAnimationFrame(function () {
            //             e.deltaY
            //             app.isScrolling = false;
            //         });

            //         app.isScrolling = true;
            //     }
            // }
        });

        $(document).on("keyup touchend", (e) => {
            app.allowToScroll = true
            app.touchPointY = undefined
        })
    },

    updateSectionPie: (index) => {
        let datasets = app.sectionPie.config.data.datasets;
        //Tìm xem có ô nào đang active không
        for (var i = 0; i < datasets[0].backgroundColor.length; i++) {
            if (datasets[0].backgroundColor[i] === '#ef2821') {
                datasets[0].backgroundColor[i] = '#8e8e8e';
                break;
            }
        }
        //Set khung active mới
        datasets[0].backgroundColor[index] = '#ef2821';
        app.sectionPie.config.data.datasets = datasets;
        app.sectionPie.update();
    },
    /**
     * event khi có nút bấm chuyển section được bấm
     * @param {Event} event event của nút bấm
     * @param {number} sectionNum section cần chuyển đến
     */
    toSection: (event, sectionNum) => {
        event.preventDefault();

        if (app.isMobile) {
            $('#navbarSupportedContent').fadeOut();
        }

        app.changeSection(sectionNum);
    },

    /**
     * Chuyển section
     * @param {number} sectionNum
     */
    changeSection: (sectionNum) => {
        //Chỉnh lại active nav bar
        let activeLi = document.querySelector(".nav-item.active");
        if (activeLi != null) {
            activeLi.classList.remove("active");
            activeLi = undefined;
        }

        if (sectionNum === 1) {
            //Về trang chủ
            activeLi = document.querySelector('.nav-item[data-nav-section="1"]');
        }
        if ([2, 3, 4, 7, 8].includes(sectionNum)) {
            //Các trang dịch vụ
            activeLi = document.querySelector('.nav-item[data-nav-section="23478"]');
        }
        if (sectionNum === 6) {
            //Về trang chủ
            activeLi = document.querySelector('.nav-item[data-nav-section="6"]');
        }

        if (typeof activeLi != "undefined" && activeLi != null) {
            activeLi.classList.add("active");
        }

        // show section nav
        // let sectionNavShowButton = document.querySelector('.section-navigator__button-show');
        // console.log(sectionNavShowButton);
        // let sectionNav = document.querySelector('.section-navigator');
        // sectionNavShowButton.addEventListener('click', function() {
        //     sectionNav.classList.add('section-nav-show');
        // });

        //Chỉnh lại ẩn hiện và active section navigator 
        let sectionNav = document.querySelector('.section-navigator');
        if ([2, 3, 4, 7, 8].includes(sectionNum)) {
            let sectionPieNum = (sectionNum <= 4 && sectionNum >= 2) ? sectionNum : sectionNum - 4;
            app.updateSectionPie(sectionPieNum);
            //Các trang dịch vụ thì hiện
            sectionNav.classList.remove('d-none');
            //bỏ active trước
            let activeBtn = document.querySelectorAll(".btn-section-navigator.active");
            //let activeBtnMobile = document.querySelector('.pie.active');
            //queryselectorAll
            for (let i = 0; i < activeBtn.length; i++) {
                if (activeBtn[i] != null) {
                    activeBtn[i].classList.remove('active');
                }
            }

            //if (activeBtnMobile != null) {
            //    activeBtnMobile.classList.remove('active');
            //}
            //Lấy button cần active
            activeBtn = document.querySelectorAll(`.btn-section-navigator[data-nav-section="${sectionNum}"]`);

            for (let i = 0; i < activeBtn.length; i++) {
                activeBtn[i].classList.add('active');

            }

            //Lấy button cần active
            //activeBtnMobile = document.querySelector(`.pie[data-nav-section="${sectionNum}"]`);
            //activeBtnMobile.classList.add('active');
        } else {
            if (!sectionNav.classList.contains('d-none')) {
                sectionNav.classList.add('d-none');
            }
        }

        //Điều hướng
        let totalSection = document.querySelectorAll("section.section");
        let currentSection = document.querySelector(".section.top");
        let nextSectionId = `section-${sectionNum}`;

        if (sectionNum > 0 && sectionNum <= totalSection.length) {
            currentSection.classList.remove("top");
            document.getElementById(nextSectionId).classList.add("top");
        }
    },

    toggleMenuMobile: () => {
        $('#navbarSupportedContent').fadeToggle();
        let navBarToggleButton = document.querySelector('.navbar-toggler');
        console.log(navBarToggleButton);
        // remove toggle button
        navBarToggleButton.classList.add('remove-btn');
        
        // add close navigator button
        // navBarCloseButton.classList.add('add-btn');
    },
    
    closeMobileNavigator: () => {
        $('#navbarSupportedContent').fadeToggle();
        let navBarToggleButton = document.querySelector('.navbar-toggler');
        console.log(navBarToggleButton);
        navBarToggleButton.classList.remove('remove-btn');
    },

    showSectionNavigator: () => {
        let sectionNavButtons = document.querySelector('.section-navigator__service-buttons');
        sectionNavButtons.classList.toggle('section-nav-show');
        // console.log(sectionNavButtons);
    },

    /**
     * Đổi ngôn ngữ
     * @param {string} lang vi, en, jv
     */
    setTextByLanguage: (event, lang) => {
        //stop event
        if (event) {
            event.preventDefault();
        }

        var name = "Vi";
        if (lang === "en") {
            name = "En";
        }
        if (lang === "jv") {
            name = "Jp";
        }

        $(".dropdown-myrmica span").html(name);

        // navigation bar
        $("#home-link").html(app.textByLang[lang].HomeLink);
        $("#service-link").html(app.textByLang[lang].ServiceLink);
        $("#about-us-link").html(app.textByLang[lang].AboutUsLink);
        $("#contact-link").html(app.textByLang[lang].ContactLink);

        // home page
        $("#section-1 h2.leading-text").html(app.textByLang[lang].Section1LeadingText);
        $("#section-1 h2.following-text").html(app.textByLang[lang].Section1FollowingText);
        $("#section-1 a.app-mobile p:nth-of-type(1)").html(app.textByLang[lang].Section1FirstMobileTitle);
        $("#section-1 a.app-mobile p:nth-of-type(2)").html(app.textByLang[lang].Section1SecondMobileTitle);
        $("#section-1 a.app-web p:nth-of-type(1)").html(app.textByLang[lang].Section1FirstWebTitle);
        $("#section-1 a.app-web p:nth-of-type(2)").html(app.textByLang[lang].Section1SecondWebTitle);
        $("#section-1 a.app-ai p:nth-of-type(1)").html(app.textByLang[lang].Section1FirstAiTitle);
        $("#section-1 a.app-ai p:nth-of-type(2)").html(app.textByLang[lang].Section1SecondAiTitle);

        $("#section-1 a.app-iot p:nth-of-type(1)").html(app.textByLang[lang].Section1IotTitle);
        $("#section-1 a.app-data p:nth-of-type(1)").html(app.textByLang[lang].Section1FirstDataTitle);
        $("#section-1 a.app-data p:nth-of-type(2)").html(app.textByLang[lang].Section1SecondDataTitle);

        $("#section-1 p.section-footer-indicator").html(app.textByLang[lang].Section1Scroll);

        // mobbile
        $("#section-2 h2.leading-text").html(app.textByLang[lang].Section2LeadingText);
        $("#section-2 h2.following-text").html(app.textByLang[lang].Section2FollowingText);
        $("#section-2 p.content").html(app.textByLang[lang].Section2Description);
        $("#section-2 a.read-more-indicator span").html(app.textByLang[lang].Section2MoreInfo);
        $("#section-2 p.section-footer-indicator").html(app.textByLang[lang].Section2Scroll);

        // website
        $("#section-3 h2.leading-text").html(app.textByLang[lang].Section3LeadingText);
        $("#section-3 p.content").html(app.textByLang[lang].Section3Description);
        $("#section-3 a.read-more-indicator span").html(app.textByLang[lang].Section3MoreInfo);
        $("#section-3 p.section-footer-indicator").html(app.textByLang[lang].Section3Scroll);

        // ai
        $("#section-4 h2.leading-text").html(app.textByLang[lang].Section4LeadingText);
        $("#section-4 h2.following-text").html(app.textByLang[lang].Section4FollowingText);
        $("#section-4 p.content").html(app.textByLang[lang].Section4Description);
        $("#section-4 a.read-more-indicator span").html(app.textByLang[lang].Section4MoreInfo);
        $("#section-4 p.section-footer-indicator").html(app.textByLang[lang].Section4Scroll);

        // iot
        $("#section-7 h2.leading-text").html(app.textByLang[lang].Section7LeadingText);
        $("#section-7 p.content").html(app.textByLang[lang].Section7Description);
        $("#section-7 a.read-more-indicator span").html(app.textByLang[lang].Section7MoreInfo);
        $("#section-7 p.section-footer-indicator").html(app.textByLang[lang].Section7Scroll);

        // contact 
        $("#section-5 h2.leading-text").html(app.textByLang[lang].Section5LeadingText);
        $("#section-5 h2.following-text").html(app.textByLang[lang].Section5FollowingText);
        $("#section-5 button.btn-myrmica-primary").html(app.textByLang[lang].Section5SendInfo);
        $("#section-5 p.section-footer-indicator").html(app.textByLang[lang].Section5Scroll);
        // $('input:text').attr('placeholder','Some New Text');
        $('#txtFullname').attr('placeholder', app.textByLang[lang].Section5FormFullName);
        $('#txtPhone').attr('placeholder', app.textByLang[lang].Section5FormPhonenumber);
        $('#txtEmail').attr('placeholder', app.textByLang[lang].Section5FormEmail);
        $('#txtRequest').attr('placeholder', app.textByLang[lang].Section5FormRequest);

        // about us
        $('#section-6 .about-us-content .about-us-content-text h2.leading-text').html(app.textByLang[lang].Section6LeadingText);
        $('#section-6 .about-us-content .about-us-content-text p').html(app.textByLang[lang].Section6FollowingText);

        // footer
        $('#section-6 .about-us-footer .footer-address h3').html(app.textByLang[lang].Section6FooterCompanyName);
        $('#section-6 .about-us-footer .footer-address p:nth-of-type(1)').html(app.textByLang[lang].Section6FooterCompanyAddress);
        $('#section-6 .about-us-footer .footer-address p:nth-of-type(2)').html(app.textByLang[lang].Section6FooterCompanyLicense);
        $('#section-6 .about-us-footer .footer-service h3').html(app.textByLang[lang].Section6FooterCompanyService);
        $('#section-6 .about-us-footer .footer-service a:nth-of-type(1)').html(app.textByLang[lang].Section6FooterCompanyMobileService);
        $('#section-6 .about-us-footer .footer-service a:nth-of-type(2)').html(app.textByLang[lang].Section6FooterCompanyWebService);
        $('#section-6 .about-us-footer .footer-service a:nth-of-type(3)').html(app.textByLang[lang].Section6FooterCompanyAiService);
        $('#section-6 .about-us-footer .footer-service a:nth-of-type(4)').html(app.textByLang[lang].Section6FooterCompanyIoTService);
        $('#section-6 .about-us-footer .footer-service a:nth-of-type(5)').html(app.textByLang[lang].Section6FooterCompanyDataService);
        $('#section-6 .about-us-footer .footer-about-us h3').html(app.textByLang[lang].Section6FooterAboutUs);
        $('#section-6 .about-us-footer .footer-about-us a').html(app.textByLang[lang].Section6FooterAboutUsLink);
        $('#section-6 .about-us-footer .footer-contact h3').html(app.textByLang[lang].Section6FooterMoreInfo);




        // console.log(app.textByLang[lang]);
    },

    /**
     * Lưu contact lên server
     */
    submitContact: (event) => {
        var unindexed_array = $('#form').serializeArray();
        var formData = {};
        $.map(unindexed_array, function(n, i) {
            formData[n['name']] = n['value'];
        });

        var hasError = false;
        var errorMessage = '';
        if (!formData.fullname.length) {
            errorMessage = 'Không được để trống họ tên';
            hasError = true;
        }

        $.ajax({
            url: '/Home/SubmitContact',
            method: 'POST',
            data: formData
        }).done(response => {
            if (response === 'success') {
                //thành công
            } else {
                //failed
            }
        }).fail(error => {
            console.log(error);
        });

        return false;
    }
};

app.init();
